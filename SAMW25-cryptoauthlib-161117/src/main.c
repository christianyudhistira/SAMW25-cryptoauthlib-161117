/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * This is a bare minimum user application template.
 *
 * For documentation of the board, go \ref group_common_boards "here" for a link
 * to the board-specific documentation.
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to system_init()
 * -# Basic usage of on-board LED and button
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <string.h>
#include "cryptoauthlib.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAMW25 ECC --"STRING_EOL \
"-- "BOARD_NAME " --"STRING_EOL	\
"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

static struct usart_module cdc_uart_module;

static void configure_console(void)
{
	struct usart_config usart_conf;

	usart_get_config_defaults(&usart_conf);
	usart_conf.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	usart_conf.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	usart_conf.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	usart_conf.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	usart_conf.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;
	usart_conf.baudrate    = 115200;

	stdio_serial_init(&cdc_uart_module, EDBG_CDC_MODULE, &usart_conf);
	usart_enable(&cdc_uart_module);
}

int main (void)
{
	system_init();
	configure_console();
	
	printf(STRING_HEADER);
	
	atcab_init(&cfg_ateccx08a_i2c_default);
	
	uint8_t revision[4];
	char displaystr[15];
	int displaylen = sizeof(displaystr);
	ATCA_STATUS status;
	
	status = atcab_info(revision);
	atcab_release();
	if ( status == ATCA_SUCCESS ) {
		// dump revision
		atcab_bin2hex(revision, 4, displaystr, &displaylen );
		printf("\r\n");
		printf("\r\nrevision:\r\n%s", displaystr);
	}
// 	atcab_info(revision);
// 	// dump revision
// 	atcab_bin2hex(revision, 4, displaystr, &displaylen );
// 	printf("\r\nrevision:\r\n%s\r\n", displaystr);

}
